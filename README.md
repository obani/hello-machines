# Hello Machines

A simple client/server program which permits clients to communicate their IP/Hostname to a server, and write them in a logfile

## Build

Client : `make client`
Server : `make server`

## Execute

Client : `./client [server address]`
Server address must be of type *addr:port*

Server : `./server [port number] [log file]`
Port number must be between 0 and 65535 *(defaults to 4444)*
Log file is the file in which the logs will be written *(defaults to logs.txt)*