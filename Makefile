COMMAND=go build

all: client server

client: src/common.go src/client.go
	$(COMMAND) -o $@ $^

server: src/common.go src/server.go
	$(COMMAND) -o $@ $^

clean:
	rm -rf client server *.txt