package main

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"time"
)

const (
	WAIT_TIME 		time.Duration	= time.Second * 2
	DEFAULT_FILE	string			= "logs.txt"
)

func Fatal(str string) {
	fmt.Println(str)
	fmt.Printf("Usage : %s [port number] [log file]\n", os.Args[ 0 ])
	os.Exit(1)
}

func main() {
	var port int
	var log_file string
	var err error

	switch len( os.Args ) {
	case 3:
		port, err = strconv.Atoi(os.Args[ 1 ])
		if err != nil || port < 0 || port > (1 << 16) {
			Fatal("Invalid port number")
		}
		log_file = os.Args[ 2 ]
	case 2:
		port = DEFAULT_PORT
		log_file = os.Args[ 1 ]
	case 1:
		port = DEFAULT_PORT
		log_file = DEFAULT_FILE
	default:
		Fatal("Invalid arguments")
	}

	f, err := os.Create(log_file)
	ErrorASSERT(err)

	my_addr, err := net.ResolveUDPAddr(DEFAULT_NETWORK, fmt.Sprintf(":%d", port))
	ErrorASSERT(err)
	conn, err := net.ListenUDP(DEFAULT_NETWORK, my_addr)
	ErrorASSERT(err)

	fmt.Println("Started server on port", port, "with log file", log_file)

	input := make([]byte, 512)
	for true {
		conn.SetReadDeadline(time.Now().Add(WAIT_TIME))
		read_bytes, addr, err := conn.ReadFromUDP(input)

		if read_bytes == 0 {
			continue
		}

		var str string
		{
			num_interfaces := uint8(input[0])
			i := 1
			for inter := uint8(0); inter < num_interfaces; inter++ {
				l := uint8(input[i])
				h := net.HardwareAddr(input[(i+1):(int(l)+i+1)])
				str += h.String() + " "
				i += int(l) + 1
			}
			str += addr.IP.String() + " "
			str += string(input[i:read_bytes]) + "\n"
		}
		
		_, err = f.WriteString(str)
		ErrorASSERT(err)
		fmt.Print(str)
	}
}