package main

import (
	"fmt"
	"net"
	"os"
	"strings"
)

func Fatal(str string) {
	fmt.Println(str)
	fmt.Printf("Usage : %s [server address]\n", os.Args[ 0 ])
	os.Exit(1)
}

func main() {
	if len( os.Args ) != 2 {
		Fatal("Invalid arguments")
	}

	addr_in := os.Args[ 1 ]

	server_addr, err := net.ResolveUDPAddr(DEFAULT_NETWORK, addr_in)
	ErrorASSERT(err)	
	conn, err := net.DialUDP(DEFAULT_NETWORK, nil, server_addr)
	ErrorASSERT(err)

	var info Info

	interfaces, err := net.Interfaces()
	ErrorASSERT(err)
	fmt.Println("MAC Addresses:")
	for _, inter := range interfaces {
		addrs, err := inter.Addrs()
		ErrorASSERT(err)
		if strings.HasPrefix( inter.Name, "e" ) {
			for _, addr := range addrs {
				ipnet, ok := addr.(*net.IPNet)
				if ok && !ipnet.IP.IsLoopback() && ipnet.IP.To4() != nil {
					info.NumInterfaces += 1
					new_inter := Interface{uint8(len(inter.HardwareAddr)), inter.HardwareAddr}
					info.Interfaces = append(info.Interfaces, Interface{uint8(len(inter.HardwareAddr)), inter.HardwareAddr})
					fmt.Println(" -", new_inter.Addr.String(), new_inter.Size, "bytes")
				}
			}
		}
	}

	hostname, err := os.Hostname()
	ErrorASSERT(err)
	info.Name = hostname

	fmt.Println("hostname :", info.Name)

	var request []byte
	{
		request = append(request, byte(info.NumInterfaces))
		for _, inter := range info.Interfaces {
			request = append(request, byte(inter.Size))
			request = append(request, inter.Addr...)
		}
		request = append(request, ([]byte(info.Name))...)
	}

	n, err := conn.Write(request)

	if n != len(request) {
		fmt.Println("Didn't write everything, expected", n, "got", len(request))
		return
	}

	fmt.Println("Done")
}