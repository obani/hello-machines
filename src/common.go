package main

import (
	"log"
	"net"
)

const (
	DEFAULT_NETWORK	string			= "udp4"
	DEFAULT_PORT	int				= 4444
)

type Interface struct {
	Size		uint8
	Addr		net.HardwareAddr
}

type Info struct {
	NumInterfaces	uint8
	Interfaces 		[]Interface
	Name 			string
}

func ErrorASSERT(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

